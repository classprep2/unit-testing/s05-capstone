const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http'); 
chai.use(http);

describe('forex/ForeignExchange_API_test_suites', ()=>{
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:4000').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:4000')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})

	it('test_api_get_rates_returns_object_of_size_6', (done) => {
		chai.request('http://localhost:4000')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(6); // the length argument means 6 documents in an object
			done();	
		})		
	})


	// 1. Check if post /currency is running
	it('test_api_get_currency_is_running', () => {
		chai.request('http://localhost:4000').get('/currency')
		.end((err, res) => {
			expect(res.status).to.equal(200);
		})
	})

	// 2. Check if post /currency returns status 400 if name is missing
	it('test_api_post_currency_returns_400_if_no_NAME', (done) => {		
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
		    alias: 'yecoin',
	      	ex: {
		        'peso': 550.73,
		        'won': 1190.24,
		        'yen': 120.63,
		        'yuan': 70.03
	      	}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('test_api_post_currency_returns_400_if_name_is_not_a_string', (done) => {		
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
		    alias: 'yecoin',
		    name: 101, 
	      	ex: {
		        'peso': 550.73,
		        'won': 1190.24,
		        'yen': 120.63,
		        'yuan': 70.03
	      	}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('test_api_post_currency_returns_400_if_name_is_an_empty_string', (done) => {		
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
		    alias: 'yecoin',
		    name: "",
	      	ex: {
		        'peso': 550.73,
		        'won': 1190.24,
		        'yen': 120.63,
		        'yuan': 70.03
	      	}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('test_api_post_currency_returns_400_if_name_is_not_a_string', (done) => {		
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
		    alias: 'yecoin',
		    name: true,
	      	ex: {
		        'peso': 550.73,
		        'won': 1190.24,
		        'yen': 120.63,
		        'yuan': 70.03
	      	}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})
	

	it('test_api_post_currency_returns_400_if_no_EX_property', (done) => {		
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
			alias: 'yecoin',
		     	name: 'Yekok Coin'
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('test_api_post_currency_returns_400_if_ex_is_not_an_object', (done) => {		
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
			alias: 'yecoin',
	      	name: 'Yekok Coin',
	      	ex: 550.73
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})


	// 7. Check if post /currency returns status 400 if ex is empty
	it('test_api_post_currency_returns_400_if_ex_isEmpty', (done) => {		
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
			alias: 'yecoin',
			name: 'Yekok Coin',
	      	ex: {}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	// 8. Check if post /currency returns status 400 if alias is missin
	it('test_api_post_currency_returns_400_if_no_ALIAS', (done) => {		
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
			name: 'Yekok Coin',
	      	ex: {
		        'peso': 550.73,
		        'won': 1190.24,
		        'yen': 120.63,
		        'yuan': 70.03
	      	}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})
	
	// 9. Check if post /currency returns status 400 if alias is not an string
	it('test_api_post_currency_returns_400_if_alias_not_a_string', (done) => {		
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
			name: 'Yekok Coin',
			alias: 101,
	      	ex: {
		        'peso': 550.73,
		        'won': 1190.24,
		        'yen': 120.63,
		        'yuan': 70.03
	      	}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	// 10. Check if post /currency returns status 400 if alias is empty
	
	it('test_api_post_currency_returns_400_if_currency_alias_is_an_empty_string', (done) => {		
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
			name: 'Yekok Coin',
			alias: 101,
	      	ex: {
		        'peso': 550.73,
		        'won': 1190.24,
		        'yen': 120.63,
		        'yuan': 70.03
	      	}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})	


	// 11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias

	it('test_api_post_currency_returns_400_if_alias_duplicate-found', (done) => {
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
			alias: 'yecoin'
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// 12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates

	it('test_api_post_currency_returns_200_if_correct_input_no_duplicate', (done) => {
		chai.request('http://localhost:4000')
		.post('/currency')
		.type('json')
		.send({
			alias: 'Nameso',
			name: 'Namek Coin',
			ex: {
				'peso': 0.17,
		        'usd': 0.89,
		        'won': 12.8,
		        'yuan': 100
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		})
	})

	
})

