const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});


	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})


	


	// Capstone 5
	// Check if post /currency is running

	app.get('/currency', (req, res) => {
		return res.status(200).send({
			'success': '200 - you are now to currency input validations'
		})
	})

	app.post('/currency' , (req,res) =>{
		// 2. Check if post /currency returns status 400 if name is missing
		if(!req.body.hasOwnProperty('name')){
				return res.status(400).send({
				'error': 'Bad Request - missing required parameter NAME'
			})
		}

		// 3. Check if post /currency returns status 400 if name is not a string
		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - NAME should be a string'
			})
		}

		// 4. Check if post /currency returns status 400 if name is empty 
		if(req.body.name === ""){
			return res.status(400).send({
				'error': 'Bad Request - NAME should not be empty'
			})
		}

		// 5. Check if post /currency returns status 400 if ex is missing
		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter EX'
			})	
		}


		// 6. Check if post /currency returns status 400 if ex is not an object
		if(typeof req.body.ex !== "object"){
			return res.status(400).send({
				'error': 'Bad Request - No document input, EX should be an object'
			})	
		}

		// 7. Check if post /currency returns status 400 if ex is empty
		if(Object.keys(req.body.ex).length === 0){
			return res.status(400).send({
				'error': 'Bad Request - No currencies input detected, include currencies'
			})	
		}

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter ALIAS'
			})	
		}

		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - ALIAS should be a string'
			})	
		}

		if(req.body.alias === ''){
			return res.status(400).send({
				'error': 'Bad Request - ALIAS has no input / empty string'
			})	
		}
			
		if(req.body.hasOwnProperty('alias') && req.body.hasOwnProperty('name') && req.body.hasOwnProperty('ex')){
			if(exchangeRates.hasOwnProperty(req.body.alias)){
				return res.status(400).send({
					message: 'Duplicate Found'
				})
			}
			return res.status(200).send({
				message: 'All fields are complete and no duplicates'
			})
		}



	})



}